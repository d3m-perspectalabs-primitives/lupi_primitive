from .lupisvm import LupiSvmClassifier



__version__ = 'v3.0.0'
__author__ = 'VencoreLabs'

__all__ = ['LupiSvmClassifier']

from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)  # type: ignore

