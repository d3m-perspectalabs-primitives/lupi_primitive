from typing import Any, List, Dict, Union, Optional, Sequence, Tuple
import typing
from numpy import ndarray
from collections import OrderedDict
import os.path
from sklearn.impute import SimpleImputer as Imputer

from d3m.metadata import hyperparams, params
from d3m.metadata import base as metadata_base
from d3m import utils, container
from d3m.container import DataFrame as d3m_dataframe

from d3m.container import DataFrame as d3m_DataFrame
from d3m.container.numpy import ndarray as d3m_ndarray
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
import common_primitives.utils as common_utils

import numpy as np
import pandas as pd
from scipy import sparse

from sklearn.model_selection import GridSearchCV, ParameterGrid
from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing

from sklearn.ensemble import RandomForestClassifier
import math
from sklearn.utils.validation import check_X_y, check_array
from sklearn.utils.validation import indexable, check_is_fitted
from sklearn.svm import SVR, SVC
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import LinearRegression
from sklearn.base import TransformerMixin, BaseEstimator
import sys
import logging
log = logging.getLogger(__name__)
import warnings
warnings.filterwarnings("ignore")


__author__='VencoreLabs'
__version__="v3.0.0"
D3M_API_VERSION = '2019.5.8'

Inputs = d3m_dataframe
Outputs = d3m_dataframe

class Params(params.Params):
    support_: Optional[ndarray]
    support_vectors_: Optional[ndarray]
    n_support_: Optional[ndarray]
    dual_coef_: Optional[ndarray]
    coef_: Optional[ndarray]
    intercept_: Optional[ndarray]
    _sparse: Optional[bool]
    shape_fit_: Optional[tuple]
    _dual_coef_: Optional[ndarray]
    _intercept_: Optional[ndarray]
    probA_: Optional[ndarray]
    probB_: Optional[ndarray]
    _gamma: Optional[float]
    classes_: Optional[ndarray]
    target_names_: Optional[Sequence[Any]]

class Hyperparams(hyperparams.Hyperparams):
    C = hyperparams.Bounded[float](
        default=1,
        lower=0,
        upper=None,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        description='Penalty parameter C of the error term.'
    )
    kernel = hyperparams.Enumeration[str](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/TuningParameter"],
        values=['linear', 'poly', 'rbf', 'sigmoid', 'precomputed'],
        default='rbf',
        description='Specifies the kernel type to be used in the algorithm. It must be one of \'linear\', \'poly\', \'rbf\', \'sigmoid\', \'precomputed\' or a callable. If none is given, \'rbf\' will be used.'
    )
    degree = hyperparams.Bounded[int](
        default=3,
        lower=0,
        upper=None,
        description='Degree of the polynomial kernel function (\'poly\'). Ignored by all other kernels.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    gamma = hyperparams.Union(
        configuration=OrderedDict({
            'float': hyperparams.Bounded[float](
                default=0.1,
                lower=0,
                upper=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'auto': hyperparams.Constant(
                default='auto',
                description='1/n_features will be used.',
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='auto',
        description='Kernel coefficient for \'rbf\', \'poly\' and \'sigmoid\'. If gamma is \'auto\' then 1/n_features will be used instead.  coef0 : float, optional (default=0.0) Independent term in kernel function. It is only significant in \'poly\' and \'sigmoid\'.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    coef0 = hyperparams.Hyperparameter[float](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/TuningParameter"],
        default=0,
    )
    probability = hyperparams.Hyperparameter[bool](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/TuningParameter"],
        default=False,
        description='Whether to enable probability estimates. This must be enabled prior to calling `fit`, and will slow down that method. '
    )
    shrinking = hyperparams.Hyperparameter[bool](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/TuningParameter"],
        default=True,
        description='Whether to use the shrinking heuristic. '
    )
    tol = hyperparams.Bounded[float](
        default=0.001,
        lower=0,
        upper=None,
        description='Tolerance for stopping criterion.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    class_weight = hyperparams.Hyperparameter[Union[str, dict]](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/TuningParameter"],
        default='balanced',
        description='Set the parameter C of class i to class_weight[i]*C for SVC. If not given, all classes are supposed to have weight one. The "balanced" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))`` '
    )
    max_iter = hyperparams.Hyperparameter[int](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/ControlParameter"],
        default=-1,
        description='Hard limit on iterations within solver, or -1 for no limit. '
    )
    n_jobs = hyperparams.Hyperparameter[int](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/ControlParameter"],
        default=4,
        description='Number of jobs to run in parallel. '
    )
    gamma_gridsearch = hyperparams.Hyperparameter[Tuple[float, float, float]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        default=(-1.0, 6.5, 3.4),
        description='Specifiy the range and step for the primitive internal grid search on gamma parameter. Expecting a tuple (lower bound, upper bound, step)'
    )
    C_gridsearch = hyperparams.Hyperparameter[Tuple[float, float, float]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        default=(-1.0, 6.5, 3.4),
        description='Specifiy the range and step for the primitive internal grid search on C parameter. Expecting a tuple (lower bound, upper bound, step)'
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='new',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
    )
    use_semantic_types = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
    )
    add_index_columns = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


class LupiSvmClassifier(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    This is an implementation of the LUPISVM primitive. This is the sklearn implementation of the LUPI primitive,
    which can solve the *same* binary and multiclass classification tasks as standard sklearn.svm, except that privileged
    information (i.e., additional features available for training dataset, but absent for test dataset) is present.
    The training dataset should provide a list of the indices indicating the features are privileged.
    The test data (or the unlabeled data to predict) should not have the privileged features.
    The values of the features in dataset need to be numerical, the label classes to be predicted should be categorical,
    and the missing values need to be imputed. The current implementation solves binary classification tasks.
    Future versions will also solve multiclass classification tasks.
    """
    __git_commit__ = utils.current_git_commit(os.path.dirname(__file__))
#    __git_commit__ = 'f04ec70840b317dcbc796e561b4dfa52da1135d4'
    metadata = metadata_base.PrimitiveMetadata({
        "algorithm_types": ['SUPPORT_VECTOR_MACHINE'],
        "name": "lupi_svm.LupiSvmClassifier",
        "description": "This is an implementation of the LUPISVM primitive. This is the sklearn implementation of the LUPI primitive, which can solve the *same* binary and multiclass classification tasks as standard sklearn.svm, except that privileged information (i.e., additional features available for training dataset, but absent for test dataset) is present. The training dataset should provide a list of the indices indicating the features are privileged. The test data (or the unlabeled data to predict) should not have the privileged features.   The values of the features in dataset need to be numerical, the label classes to be predicted should be categorical, and the missing values need to be imputed. The current implementation solves binary classification tasks. Future versions will also solve multiclass classification tasks.",
        "primitive_family": "CLASSIFICATION",
        "python_path": "d3m.primitives.classification.lupi_svm.LupiSvmClassifier",
        "source": {
            'name': __author__,
            'contact': 'mailto:plin@perspectalabs.com',
            'uris': ['https://gitlab.com/d3m-perspectalabs-primitives/lupi_primitive/blob/master/lupi_svm/lupisvm.py',
                     'https://gitlab.com/d3m-perspectalabs-primitives/lupi_primitive.git']
        },
        "version": __version__,
        "id": "3fd60679-cc91-3703-9ce5-34098babbbdc",
        'installation': [{'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/d3m-perspectalabs-primitives/lupi_primitive.git@{git_commit}#egg=lupi_svm'.format(
                            git_commit=__git_commit__,
                            ),
                          }]
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._lupi_svm = SVC(
            C=hyperparams['C'],
            kernel=hyperparams['kernel'],
            degree=hyperparams['degree'],
            gamma=hyperparams['gamma'],
            coef0=hyperparams['coef0'],
            shrinking=hyperparams['shrinking'],
            probability=hyperparams['probability'],
            tol=hyperparams['tol'],
            class_weight=hyperparams['class_weight'],
            max_iter=hyperparams['max_iter'],
            verbose=_verbose,
            random_state=random_seed)

        self._training_inputs: Inputs = None
        self._training_outputs: Outputs = None
        self._fitted: bool = False
        self._lupiregressor = None
        self._selregressor = None
        self._gs_estimator = None
        self._C_gridsearch = hyperparams['C_gridsearch']
        self._gamma_gridsearch = hyperparams['gamma_gridsearch']
        self._nJobs = hyperparams['n_jobs']
        self._training_indices = None

    def set_training_data(self, *, inputs: Inputs) -> None:
        #privileged_features = inputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrivilegedData')
        privileged_features = self._get_privileged_features(inputs)

        training_inputs_orig, self._training_indices = self._get_columns_to_fit(inputs, self.hyperparams)

        (dataLabelEncoders, stdDataLabelEncoders, targetsLabelEncoder, trainData_df, targetCatLabel, featureDrop) \
            = self._data_prep(training_inputs_orig, privileged_features)

        trainData_df.metadata = trainData_df.metadata.update((), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
            'privileged_features': privileged_features,
            'trainTargetsCatLabel': targetCatLabel
        })

        trainTargetsCatLabel = trainData_df.metadata.query(()).get('trainTargetsCatLabel')
        training_outputs = trainData_df[trainTargetsCatLabel]
        training_inputs = trainData_df.copy()

        training_inputs.drop(['d3mIndex'], axis=1, inplace=True)
        training_inputs.drop([trainTargetsCatLabel], axis=1, inplace=True)
        self._training_inputs = training_inputs
        self.privileged_features = trainData_df.metadata.query(()).get('privileged_features')
        self._training_outputs = training_outputs
        self.dataLabelEncoders = dataLabelEncoders
        self.stdDataLabelEncoders = stdDataLabelEncoders
        self.targetsLabelEncoder = targetsLabelEncoder
        self.featureDrop = featureDrop
        self._fitted = False

    def _get_privileged_features(self, inputs):
        privileged_features = []
        privileged_semantic_type = 'https://metadata.datadrivendiscovery.org/types/PrivilegedData'
        col_length = inputs.metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
        for col in range(col_length):
            semantic_types = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col))['semantic_types']
            col_name = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col))['name']
            if privileged_semantic_type in semantic_types:
                #                print("col {} {} is privileged".format(col, col_name))
                privileged_features.append(col_name)

        return privileged_features

    def _data_imputer(self, inputs):
        inputData = inputs.copy()
        df = inputData.apply(pd.to_numeric, args=('coerce',))
        isallnull = df.isnull().all()
        allnull_columns = isallnull.index[isallnull].tolist()
        #set all nan columns to value 0 so that the whole column will not be imputed
        df.loc[:, allnull_columns] = 0
        imputer = Imputer(missing_values=np.nan, strategy='mean')
        imputer.fit(df)
        imputed_df = pd.DataFrame(imputer.transform(df))
        imputed_df.columns = df.columns
        imputed_df.index = df.index
        return imputed_df

    def _data_prep(self, inputs, privileged_features):
        inputData = self._data_imputer(inputs)
        inputData = inputData.fillna('0').replace('', '0')
        inputData = d3m_dataframe(inputData)
        # Encode the categorical data in training data
        dataCatLabels = []
        dataLabelEncoders = dict()
        targetCatLabel = ''
        targetsLabelEncoder = preprocessing.LabelEncoder()

        categoricaldata_semantic_type = "https://metadata.datadrivendiscovery.org/types/CategoricalData"
        attribute_semantic_type = "https://metadata.datadrivendiscovery.org/types/Attribute"
        suggestedtarget_semantic_type = "https://metadata.datadrivendiscovery.org/types/SuggestedTarget"

        col_length = inputData.shape[1]
        for col_index in range(col_length):
            col_metadata = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col_index))
            name = col_metadata['name']
            structural_type = col_metadata['structural_type']
            semantic_types = col_metadata['semantic_types']

            if categoricaldata_semantic_type in semantic_types and attribute_semantic_type in semantic_types:
                dataCatLabels.append(name)
                dataLabelEncoders[name] = preprocessing.LabelEncoder().fit(inputData[name])
                inputData[name] = dataLabelEncoders[name].transform(inputData[name])
            elif categoricaldata_semantic_type in semantic_types and suggestedtarget_semantic_type in semantic_types:
                targetCatLabel = name
                self._target_names = targetCatLabel
                targetsLabelEncoder = targetsLabelEncoder.fit(inputData[name])
                targets = targetsLabelEncoder.transform(inputData[name])
                inputData[name] = targetsLabelEncoder.transform(inputData[name])
        stdDataLabelEncoders = dataLabelEncoders.copy()
        
        if len(privileged_features) != 0:
            for i in privileged_features:
                if i in stdDataLabelEncoders:
                    del stdDataLabelEncoders[i]
        #featureDrop = ["TBG_measured", "TBG"]
        #inputData.drop(featureDrop, axis=1, inplace=True)
        featureDrop = []
        return (dataLabelEncoders, stdDataLabelEncoders, targetsLabelEncoder, inputData, targetCatLabel, featureDrop)


    def _lupi_fit(self,pri_features, regr_param_grid, svc_param_grid, nJobs=1):
        pri_features = list(pri_features)

        lupiregressor = LupiRegressor(pri_features=pri_features, nJobs=nJobs, regr_type_list=['linear'],
                                      regr_param_grid=regr_param_grid, cv=6)

        lupiregressor.fit(self._training_inputs, self._training_outputs)  # fit to get the lupi regrs from the train data

        self._lupiregressor = lupiregressor

        scaler = StandardScaler()
        X_train = self._training_inputs.copy()
        X_train = scaler.fit_transform(X_train)
        X_train_lupilearned = lupiregressor.transform(X_train)
        X_train_transfered = X_train_lupilearned

        gs_estimator = GridSearchCV(self._lupi_svm, verbose=False, scoring='f1',
                                    cv=6, n_jobs=nJobs, param_grid=svc_param_grid)

        y_train = self._training_outputs
        y_train = y_train.astype(int)
        gs_estimator.fit(X_train_transfered, y_train)
        self._gs_estimator = gs_estimator
        self._fitted = True

    def _sel_svm(self, X_train_all, y_train,
                ran_seed=0, cv_score='f1',
                nJobs=1, regr_type=None,
                regr_param_grid={},
                svc_param_grid={},
                cv=6,
                regr_weighted=True):

        X_train = X_train_all.copy()
        X_train = X_train.reset_index(drop=True)
        y_train = y_train.reset_index(drop=True)

        if regr_type == 'ln_sel':
            selregressor = SelRegressor(nJobs=nJobs, regr_type_list=['linear'],
                                        regr_param_grid=regr_param_grid, cv=6)

            selregressor.fit(self._training_inputs,
                             self._training_outputs)  # fit to get the lupi regrs from the train data
            log.info("  regr_type:{}   choosen priv features:{}".format(regr_type, selregressor.choosen_sel_scree['scree']))

            X_train_learned = selregressor.transform(X_train)
        elif regr_type == 'nl_sel':
            selregressor = SelRegressor(nJobs=nJobs, regr_type_list=['kernelridge'],
                                        regr_param_grid=regr_param_grid, cv=6)

            selregressor.fit(self._training_inputs,
                             self._training_outputs)  # fit to get the lupi regrs from the train data
            log.info("  regr_type:{}   choosen priv features:{}".format(regr_type, selregressor.choosen_sel_scree['scree']))
            X_train_learned = selregressor.transform(X_train)

        elif regr_type == 'svm':
            scaler = StandardScaler()
#            X_train_learned = scaler.fit_transform(X_train)
            X_train_learned = X_train.copy()
            selregressor = None
        else:
            log.error("regr_type is not valid")
            sys.exit(1)

#        svc_param_grid = {'gamma':[0.0345], 'C':[1]}
#        estimator = SVC(class_weight='balanced', kernel='rbf', random_state=ran_seed)
        gs_estimator = GridSearchCV(self._lupi_svm, scoring=cv_score, cv=cv, n_jobs=nJobs, param_grid=svc_param_grid)
        gs_estimator.fit(X_train_learned, y_train)

        best_cvscore = gs_estimator.best_score_
        return gs_estimator, best_cvscore, selregressor

    def _sel_fit(self, regr_param_grid, svc_param_grid, nJobs=1):
        cv_sel_results = {
            'svm': {},
            'ln_sel': {}
        }

        for regr_type in ['ln_sel', 'svm']:
            X_train = self._training_inputs.copy()
            y_train = self._training_outputs
            #y_train = y_train.astype(int)

            gs_estimator, best_cvscore, selregressor = self._sel_svm(X_train, y_train,
                                              ran_seed=None, cv_score='f1',
                                              nJobs=nJobs, regr_type=regr_type,
                                              regr_param_grid=regr_param_grid,
                                              svc_param_grid=svc_param_grid,
                                              cv=6,
                                              regr_weighted=False)

            cv_sel_results[regr_type] = {'gs_estimator': gs_estimator, 'best_cvscore': best_cvscore, 'selregressor': selregressor}

        # convert dict to df
        er_df = pd.DataFrame.from_dict(cv_sel_results, orient='index')
        best_regr = er_df['best_cvscore'].idxmax()
        best_gs_estimator= er_df.loc[best_regr, 'gs_estimator']
        best_selregressor = er_df.loc[best_regr, 'selregressor']
        best_cvscore = er_df.loc[best_regr, 'best_cvscore']
        log.info("best regr:{}  best_cvscore:{}".format(best_regr, best_cvscore))
        self._gs_estimator = best_gs_estimator
        self._selregressor = best_selregressor
        self._fitted = True

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        nJobs = self._nJobs
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        pri_features_names = self.privileged_features
        pri_features = [ self._training_inputs.columns.get_loc(x) for x in pri_features_names ]

        log.info("self._gamma_gridsearch {}".format(self._gamma_gridsearch))
        log.info("self._C_gridsearch {}".format(self._C_gridsearch))

        regr_param_grid_dict = dict(gamma_list=self._gamma_gridsearch, C_list=self._C_gridsearch)
        C_list = regr_param_grid_dict['C_list']
        gamma_list = regr_param_grid_dict['gamma_list']
        C_range = 2.0 ** np.arange(C_list[0], C_list[1], C_list[2])
        gamma_range = 1.0 / (2 * ((2 ** np.arange(gamma_list[0], gamma_list[1], gamma_list[2])) ** 2))
        alpha_range = [1. / (2. * c) for c in C_range]
        regr_param_grid = dict(alpha=alpha_range, gamma=gamma_range)

        svc_param_grid_dict = dict(gamma_list=self._gamma_gridsearch, C_list=self._C_gridsearch)
        C_list = svc_param_grid_dict['C_list']
        gamma_list = svc_param_grid_dict['gamma_list']
        C_range = 2.0 ** np.arange(C_list[0], C_list[1], C_list[2])
        gamma_range = 1.0 / (2 * ((2 ** np.arange(gamma_list[0], gamma_list[1], gamma_list[2])) ** 2))
        svc_param_grid = dict(gamma=gamma_range, C=C_range)

        if len(pri_features) > 0: # dataset has privileged features
            self._lupi_fit(pri_features, regr_param_grid, svc_param_grid, nJobs=nJobs)
        else:  # datasett has no privileged feature
            self._sel_fit(regr_param_grid, svc_param_grid, nJobs=nJobs)
        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        stdDataLabelEncoders = self.stdDataLabelEncoders
        inputData = self._data_imputer(inputs)
        inputData = d3m_dataframe(inputData)
        inputData = inputData.fillna('0').replace('', '0')

        targetCatLabel = None

        categoricaldata_semantic_type = "https://metadata.datadrivendiscovery.org/types/CategoricalData"
        attribute_semantic_type = "https://metadata.datadrivendiscovery.org/types/Attribute"
        suggestedtarget_semantic_type = "https://metadata.datadrivendiscovery.org/types/SuggestedTarget"

        col_length = inputData.shape[1]
        for col_index in range(col_length):
            col_metadata = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col_index))
            name = col_metadata['name']
            structural_type = col_metadata['structural_type']
            semantic_types = col_metadata['semantic_types']
            if categoricaldata_semantic_type in semantic_types and suggestedtarget_semantic_type in semantic_types:
                targetCatLabel = name

        test_inputs = inputData.copy()
        privileged_features = self.privileged_features

        if targetCatLabel:
            test_inputs.drop([targetCatLabel], axis=1, inplace=True)

        test_inputs.drop(['d3mIndex'], axis=1, inplace=True)
        #test_inputs.drop(self.featureDrop, axis=1, inplace=True)
        #test_inputs.drop([privileged_features], axis=1, inplace=True)

        if len(privileged_features) > 0: # dataset has privileged features
            lupiregr = self._lupiregressor
            X_test_all = test_inputs.copy()
            X_test = X_test_all.drop(list(privileged_features), axis=1, inplace=False)

            X_test_scaled = lupiregr.scaler_lupi.transform(X_test)
            X_test_lupilearned = lupiregr.transform(X_test_scaled)
            X_test_transfered = X_test_lupilearned
        else:  # datasett has no privileged feature
            selregr = self._selregressor
            X_test = test_inputs.copy()
            X_test_scaled = X_test  # already scaled
            if selregr:
                X_test_transfered = selregr.transform(X_test_scaled)
            else:
                X_test_transfered = X_test_scaled # svm case
        prediction = self._gs_estimator.best_estimator_.predict(X_test_transfered)
        if sparse.issparse(prediction):
            prediction =  prediction.toarray()
        output = d3m_dataframe( prediction, generate_metadata=False)
        column_names = [self._target_names]
        output.columns = column_names
        output.metadata = inputs.metadata.clear(for_value=output, generate_metadata=True)
        output.metadata = self._add_target_semantic_types(metadata=output.metadata, target_names=[self._target_names],
                                                          source=self)
        outputs = common_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=self.hyperparams['add_index_columns'],
                                               inputs=inputs, column_indices=self._training_indices,
                                               columns_list=[output])
        return CallResult(outputs)

    def get_params(self) -> Params:
        if not self._fitted:
            raise ValueError("Fit not performed.")
        return Params(
            support=self._lupi_svm.support_,
            support_vectors=self._lupi_svm.support_vectors_,
            n_support=self._lupi_svm.n_support_,
            dual_coef=self._lupi_svm.dual_coef_,
            coef=self._lupi_svm.coef_,
            intercept=self._lupi_svm.intercept_,
            target_names_=self._target_names
        )

    def set_params(self, *, params: Params) -> None:
        self._lupi_svm.support_ = params.support
        self._lupi_svm.support_vectors_ = params.support_vectors
        self._lupi_svm.n_support_ = params.n_support
        self._lupi_svm.dual_coef_ = params.dual_coef
        self._lupi_svm.intercept_ = params.intercept
        self._target_names = params['target_names_']
        self._fitted = False

    @classmethod
    def _get_columns_to_fit(cls, inputs: Inputs, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return inputs, list(range(len(inputs.columns)))

        inputs_metadata = inputs.metadata

        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

        columns_to_produce, columns_not_to_produce = common_utils.get_columns_to_use(inputs_metadata,
                                                                                     use_columns=hyperparams[
                                                                                         'use_input_columns'],
                                                                                     exclude_columns=hyperparams[
                                                                                         'exclude_input_columns'],
                                                                                     can_use_column=can_produce_column)
        return inputs.iloc[:, columns_to_produce], columns_to_produce
        # return columns_to_produce

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int,
                            hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        accepted_structural_types = (int, float, np.integer, np.float64)
        accepted_semantic_types = set()
        accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/Attribute")
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        semantic_types = set(column_metadata.get('semantic_types', []))

        if len(semantic_types) == 0:
            cls.logger.warning("No semantic types found in column metadata")
            return False
        # Making sure all accepted_semantic_types are available in semantic_types
        if len(accepted_semantic_types - semantic_types) == 0:
            return True

        return False

    @classmethod
    def _get_targets(cls, data: d3m_dataframe, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return data, []
        target_names = []
        target_column_indices = []
        metadata = data.metadata

        def can_produce_column(column_index: int) -> bool:
            accepted_semantic_types = set()
            accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/TrueTarget")
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = set(column_metadata.get('semantic_types', []))
            if len(semantic_types) == 0:
                cls.logger.warning("No semantic types found in column metadata")
                return False
            # Making sure all accepted_semantic_types are available in semantic_types
            if len(accepted_semantic_types - semantic_types) == 0:
                return True
            return False

        target_column_indices, target_columns_not_to_produce = common_utils.get_columns_to_use(metadata,
                                                                                               use_columns=hyperparams[
                                                                                                   'use_output_columns'],
                                                                                               exclude_columns=
                                                                                               hyperparams[
                                                                                                   'exclude_output_columns'],
                                                                                               can_use_column=can_produce_column)

        for column_index in target_column_indices:
            if column_index is metadata_base.ALL_ELEMENTS:
                continue
            column_index = typing.cast(metadata_base.SimpleSelectorSegment, column_index)
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            target_names.append(column_metadata.get('name', str(column_index)))

        targets = data.iloc[:, target_column_indices]
        return targets, target_names

    @classmethod
    def _add_target_semantic_types(cls, metadata: metadata_base.DataMetadata,
                                   source: typing.Any, target_names: List = None, ) -> metadata_base.DataMetadata:
        for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/Target',
                                                  source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                                                  source=source)
            if target_names:
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, column_index), {
                    'name': target_names[column_index],
                }, source=source)
        return metadata

class LupiRegressor(BaseEstimator, TransformerMixin):
    """ A LUPI Regressor used to learn how to generate the privileged features from the standard features.
        Parameters
        ----------
        regr_type_list : list, list of elements consist of {'linear', 'ridge', 'svr'} ,  Default option is ['linear'].
            Choose the type of regression to be used when learning how to generate the privileged features.
        param_grid: the parameter search grid for kernel='rbf' regressors 
        nJobs = int, optional , Default option is 1.
            Number of jobs to run in parallel during the training of the lupi regressor models.
        pri_features: a list of indices of privileged features in training dataset
        ----------
        input_shape : tuple
            The shape the data passed to :meth:`fit`
        """

    def __init__(self,  pri_features=[], regr_type_list=['linear'], regr_param_grid={}, nJobs=1, cv=6):
        self.pri_features = pri_features
        self.num_of_std = None
        self.cv = cv
        self.regr_list = []
        self.regr_type_list = regr_type_list
        self.scaler_lupi = None
        self.regr_param_grid = regr_param_grid
        self.nJobs = nJobs
        self.input_shape_ = None

    def _prepare_x(self, X_train):  # prepare X for fit function
        pri_features = self.pri_features
        X = check_array(X_train)
        X_pri = X[:, pri_features]
        X_std = np.delete(X, np.s_[pri_features], axis=1)
        self.num_of_std = X_std.shape[1]
        pre_X = np.concatenate((X_std, X_pri), axis=1)
        return pre_X

    def fit(self, X_train, y_train):
        """Fit the model to data.
        Parameters
        ----------
        X_train : array-like or sparse matrix of shape = [n_samples, n_features],
            The training input samples consisting of both standard and privileged features.
        y_train : None
            There is no need of a target in a transformer, yet the pipeline API
            requires this parameter. y can be anything, it does not affect the result.

        Returns
        -------
        self : object
            Returns self.
        """
        pre_X = self._prepare_x(X_train)
        X_train = check_array(pre_X)
        self.input_shape_ = X_train.shape

        if self.pri_features == []:
            log.info("pri_features is empty!!")

        if self.num_of_std == 0:
            return self
        if not isinstance(X_train, pd.DataFrame):
            X_train = pd.DataFrame(X_train)

        num_of_std = self.num_of_std

        ## first, let's get the lupi scalar for prediction
        X_stds = X_train.ix[:, 0:num_of_std - 1]
        scaler_lupi = StandardScaler()
        scaler_lupi.fit(X_stds)
        self.scaler_lupi = scaler_lupi

        ## now, fit the lupi regressors
        scaler = StandardScaler()
        X = scaler.fit_transform(X_train)
        X_stds = X[:, 0:num_of_std]

        supported_regr_types = ['svr', 'kernelridge', 'linear']
        for regr_type in self.regr_type_list:
            if regr_type not in supported_regr_types:
                log.error("not supported regr_type{}".format(regr_type))
                sys.exit(1)

                ##############################
        feature_to_regrtype = {}  # dictionary mapping jth priv feature to its regrtype

        for j in np.arange(X.shape[1] - num_of_std):
            y_true_j = X[:, num_of_std + j]
            r2score_list = []  # list to store R2 values of regr_type_list
            regr_candidate_list = []  # list to store regr candidates for j th priv feature
            for regr_type in self.regr_type_list:
                if regr_type == 'svr':
                    regr = GridSearchCV(SVR(kernel='rbf'), cv=self.cv,
                                        param_grid=self.param_grid,
                                        scoring='r2',
                                        n_jobs=self.nJobs)
                    regr.fit(X_stds, y_true_j)
                    best_cvscore = regr.best_score_
                    r2score_list.append(best_cvscore)
                    regr_candidate_list.append(regr)
                elif regr_type == 'kernelridge':
                    ridge_param_grid = self.regr_param_grid

                    regr = GridSearchCV(KernelRidge(kernel='rbf'),
                                        param_grid=ridge_param_grid,
                                        scoring='r2', cv=self.cv,
                                        n_jobs=self.nJobs)
                    regr.fit(X_stds, y_true_j)
                    best_cvscore = regr.best_score_
                    r2score_list.append(best_cvscore)
                    regr_candidate_list.append(regr)
                else:
                    regr = LinearRegression(fit_intercept=1)
                    regr.fit(X_stds, y_true_j)
                    r2_score = regr.score(X_stds, y_true_j)
                    r2score_list.append(r2_score)
                    regr_candidate_list.append(regr)
                    ##############################
            maxR2_index = r2score_list.index(max(r2score_list))  # index of the max R2 list
            best_regr = regr_candidate_list[maxR2_index]
            feature_to_regrtype[j] = self.regr_type_list[maxR2_index]
            self.regr_list.append(best_regr)

        self.feature_to_regrtype = feature_to_regrtype

        return self

    def transform(self, X, y=None):
        """ Transform the input data into standard and generated (reconstructed) privileged features.
            It transforms the data set according to the shape of the input dataset.
            The input data can be:
                1) data set with both standard and privileged features. This is the case for lupi training.
                2) data set with only standard features. This is the case for predicting of test data.
            Parameters
            ----------
            X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples which the features are in the original order. X will be pre-processed to 
            pre_x which has the features re-ordered in that the first num_of_std features are standard and  
            the next features are the privileged.

            y : numpy array of shape [n_samples]
                Target values.

            Returns
            -------
            X_transformed : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples. The first num_of_std features are standard features
             and the next features are the reconstructed privileged features.
            """
        # Check is fit had been called
        check_is_fitted(self, ['input_shape_'])

        # Input validation
        num_of_std = self.num_of_std
        if self.num_of_std == 0:
            sys.exit("Unexpected num_of_std == 0")

        if X.shape[1] == self.input_shape_[1]:  # fit dataset with both standard and privileged features
            pre_X = self._prepare_x(X)
            X = check_array(pre_X)
            X2 = X.copy()
            for j in np.arange(len(self.regr_list)):
                X2[:, num_of_std + j] = self.regr_list[j].predict(X2[:, 0:num_of_std])
        elif X.shape[1] == self.num_of_std:  # transfer test dataset with only standard features before lupi predict
            # X is scaled by lupiregr.scaler_lupi.transform(X
            X = check_array(X)
            X2 = X.copy()
            lupi_feat = np.empty([X.shape[0], len(self.regr_list)])
            X2 = np.concatenate((X2, lupi_feat), axis=1)
            for j in np.arange(len(self.regr_list)):
                X2[:, num_of_std + j] = self.regr_list[j].predict(X2[:, 0:num_of_std])
        else:
            log.error("Error: wrong input data dimension X.shape[1] = {}".format(X.shape[1]))
            sys.exit("Unexpected input data dimension of X")
        return X2


class SelRegressor(BaseEstimator, TransformerMixin):
    """ A feature augment and selection Regressor.
        Parameters
        ----------
        regr_type_list : list, list of elements consist of {'linear', 'ridge'} ,  Default option is ['linear'].
            Choose the type of regression to be used when learning how to generate the privileged features.
        param_grid: the parameter search grid for kernel='rbf' regressors 
        nJobs = int, optional , Default option is 1.
            Number of jobs to run in parallel during the training of the lupi regressor models.
        pri_features: a list of indices of privileged features in training dataset
        ----------
        input_shape : tuple
            The shape the data passed to :meth:`fit`
        """

    def __init__(self, regr_type_list=['linear'], regr_param_grid={}, nJobs=1, cv=6):
        self.num_of_std = None
        self.cv = cv
        self.regr_list = []
        self.regr_type_list = regr_type_list
        self.scaler = None
        self.regr_param_grid = regr_param_grid
        self.nJobs = nJobs
        self.input_shape_ = None
        self.choosen_sel_scree = {}

    def _train_regr(self, X_train, y_train):
        self.input_shape_ = X_train.shape
        # total number of standard features
        num_of_std = X_train.shape[1]
        self.num_of_std = num_of_std
        X_stds = X_train.copy()

        scaler = StandardScaler(copy=True, with_mean=True, with_std=True)
        scaler.fit(X_stds)
        self.scaler = scaler

        X_std_scaled = scaler.transform(X_stds)

        ## now, fit the lupi regressors
        regr_list_tmp = []
        supported_regr_types = ['kernelridge','linear']
        for regr_type in self.regr_type_list:
            if regr_type not in supported_regr_types:
                log.error("not supported regr_type{}".format(regr_type))
                sys.exit(1)

            regrs=[]
            for j in np.arange(num_of_std):
                y_true_j = X_std_scaled[:, j]
                X_stds_scaled_noj = np.delete(X_std_scaled, [j], axis = 1)

                if regr_type == 'kernelridge':
                    regr = GridSearchCV(KernelRidge(kernel='rbf'),
                                        param_grid=self.regr_param_grid,
                                        scoring='r2', cv=self.cv,
                                        n_jobs = self.nJobs)
                    regr.fit(X_stds_scaled_noj, y_true_j)
                    regrs.append(regr)
                else:
                    regr = LinearRegression(fit_intercept=1)
                    regr.fit(X_stds_scaled_noj, y_true_j)
                    regrs.append(regr)

            regr_list_tmp.extend(list(regrs))
        return (regr_list_tmp, X_std_scaled, y_train)

    def _g_scree(self, values):
        # values = np.asarray(values)
        p = len(values)
        lq_value = np.zeros(p)
        for q in range(1, p):
            mu1 = np.mean(values[0:q])
            sigma1 = np.var(values[0:q])

            mu2 = np.mean(values[q:p])
            sigma2 = np.var(values[q:p])

            sigma = (sigma1 * q + sigma2 * (p - q)) / (p - 2)

            for j in range(0, p):
                for i in range(0, q):
                    lq_value[q] = lq_value[q] \
                                  + math.log(
                        (1 / math.sqrt(2 * math.pi * sigma)) * math.exp(-((values[i] - mu1) ** 2) / (2 * sigma)))

                for i in range(q, p):
                    lq_value[q] = lq_value[q] \
                                  + math.log(
                        (1 / math.sqrt(2 * math.pi * sigma)) * math.exp(-((values[i] - mu2) ** 2) / (2 * sigma)))

        log.info("    lq_value {}".format(lq_value))
        index_best = np.argmax(lq_value)
        return index_best

    def _calc_importance_all(self, X, y, ntree=2000):
        #    forest = RandomForestClassifier(n_estimators=ntree, criterion='entropy', class_weight='balanced', random_state=0)
        forest = RandomForestClassifier(n_estimators=ntree, criterion='entropy', class_weight='balanced',
                                        random_state=0)
        forest.fit(X, y)
        importances = forest.feature_importances_
        return importances

    def _get_im_regrs_by_scree(self, X_stds, y_train, regrs, ntree=1000):
        num_of_std = X_stds.shape[1]
        num_of_features = num_of_std + len(regrs)
        sel_feature_list = list(range(num_of_std, num_of_features, 1))
        X2_std = X_stds.copy()
        sel_feat = np.empty([X_stds.shape[0], len(regrs)])
        X2 = np.concatenate((X2_std, sel_feat), axis=1)
        for j in np.arange(len(regrs)):
            X2_std_noj = np.delete(X2_std,[j], axis=1)
            X2[:, num_of_std + j] = regrs[j].predict(X2_std_noj)
        # importances of all features
        importances = self._calc_importance_all(X2, y_train, ntree=ntree)
        indices = np.argsort(importances)[::-1]
        # importances of sel features
        im_sel = importances[sel_feature_list]
        im_sel_indices = np.argsort(im_sel)[::-1]
        # find best scree index
        # sorted array
        scree_index = self._g_scree(im_sel[im_sel_indices])
        sel_scree_indices = im_sel_indices[0:scree_index]
        nregrs = np.asarray(regrs)
        sel_scree_regrs = nregrs[list(sel_scree_indices)]
        return sel_scree_regrs, sel_scree_indices

    def fit(self, X_train, y_train):
        (trained_regrs, X_stds_scaled, y_train) = self._train_regr(X_train, y_train)
        log.info("regr_list lenght: {}".format(len(trained_regrs)))
        important_regrs, sel_n_im = self._get_im_regrs_by_scree(X_stds_scaled, y_train, trained_regrs, ntree=1000)
        #important_regrs, sel_n_im = lch.get_im_allregrs_by_scree(X_stds, y_train, regr_list_tmp, ntree=500)
        self.choosen_sel_scree['scree'] = sel_n_im
        self.regr_list = important_regrs

        return self

    def transform(self, X, y=None):
        """ Transform the input data into standard and generated (reconstructed) privileged features.
            It transforms the data set according to the shape of the input dataset.
            The input data can be:
                1) data set with both standard and privileged features. This is the case for lupi training.
                2) data set with only standard features. This is the case for predicting of test data.
            Parameters
            ----------
            X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples which the features are in the original order. X will be pre-processed to 
            pre_x which has the features re-ordered in that the first num_of_std features are standard and  
            the next features are the privileged.

            y : numpy array of shape [n_samples]
                Target values.

            Returns
            -------
            X_transformed : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples. The first num_of_std features are standard features
             and the next features are the reconstructed privileged features.
            """
        # Check is fit had been called
        check_is_fitted(self, ['input_shape_'])

        # Input validation
        num_of_std = self.num_of_std
        if self.num_of_std == 0:
            sys.exit("Unexpected num_of_std == 0")
        if X.shape[1] == self.input_shape_[1]:
            X = check_array(X)
            X2 = X.copy()
            X2_o = X.copy()
            lupi_feat = np.empty([X.shape[0], len(self.regr_list)])
            X2 = np.concatenate((X2, lupi_feat), axis=1)

            for j in np.arange(len(self.regr_list)):
                X2_noj = np.delete(X2_o, [j], axis=1)
                X2[:, num_of_std + j] = self.regr_list[j].predict(X2_noj)
        else:
            log.error("Error: wrong input data dimension X.shape[1] = {}".format(X.shape[1]))
            sys.exit("Unexpected input data dimension of X")
        return X2

