import os
import re
import sys
from setuptools import setup, find_packages

PACKAGE_NAME = 'lupi_svm'

def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    assert False, "'{0}' not found in '{1}'".format(key, module_path)

setup(
    name=PACKAGE_NAME,
    version=read_package_variable('__version__'),
    description='Perspecta Labs LupiSvm',
    maintainer_email='plin@perspectalabs.com',
    maintainer='Peter Lin',
    author=read_package_variable('__author__'),
    packages=['lupi_svm'],
    install_requires=[
        'd3m',
        'scikit-learn',
    ],
    url='https://gitlab.com/d3m-perspectalabs-primitives/lupi_primitive',
    entry_points = {
        'd3m.primitives': [
            'classification.lupi_svm.LupiSvmClassifier = lupi_svm.lupisvm:LupiSvmClassifier'
        ],
    },
)


